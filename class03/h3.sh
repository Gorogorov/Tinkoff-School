#!/bin/bash
cmnd=`ps -ela | awk '/httpd/' | wc -l`
dt=`date`
if [ "$cmnd" -eq 0 ]
then
    echo "${dt} httpd was not running, starting..." >> /root/httpd_start.log
    ( exec "/root/homework/materials/class03/src/tinyhttpd/tinyhttpd/httpd" )
fi
