import psycopg2

conn = psycopg2.connect(host = "localhost", database="shop_data", user="postgres", password="qwerty")

cur = conn.cursor()

cur.execute("DROP TABLE customers CASCADE;")
cur.execute("DROP TABLE orders CASCADE;")
cur.execute("DROP TABLE goods CASCADE;")
cur.execute("DROP TABLE order_items CASCADE;")

conn.commit()

cur.close()
conn.close()

