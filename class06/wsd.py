import psycopg2

conn = psycopg2.connect(host = "localhost", database="shop_data", user="postgres", password="qwerty")

cur = conn.cursor()

f = open('db.csv', 'w')

f.write("order_item_id,order_id,quantity,first_nm,last_nm,name,vendor\n")

cur.execute("SELECT order_item_id, order_items.order_id, quantity, first_nm, last_nm, name, vendor FROM order_items INNER JOIN orders ON order_items.order_id = orders.order_id INNER JOIN customers ON orders.cust_id = customers.cust_id INNER JOIN goods ON order_items.good_id = goods.good_id ORDER BY order_item_id;")
for ordd in cur.fetchall():
    for i in range(len(ordd)):
        f.write(str(ordd[i]))
        if i != len(ordd) - 1:
            f.write(",")
    f.write("\n")


f.close()

cur.close()
conn.close()

