import psycopg2

conn = psycopg2.connect(host = "localhost", database="shop_data", user="postgres", password="qwerty")

cur = conn.cursor()

#cur.execute("DROP TABLE customers CASCADE;")
#cur.execute("DROP TABLE orders CASCADE;")
#cur.execute("DROP TABLE goods CASCADE;")
#cur.execute("DROP TABLE order_items CASCADE;")

cur.execute("CREATE TABLE customers (cust_id serial PRIMARY KEY, first_nm varchar(100), last_nm varchar(100));")
cur.execute("INSERT INTO customers (first_nm, last_nm) VALUES (%s, %s)", ("Vanya", "Ivanov"))
cur.execute("INSERT INTO customers (first_nm, last_nm) VALUES (%s, %s)", ("Anton", "Eotov"))

cur.execute("CREATE TABLE orders (order_id serial PRIMARY KEY, cust_id INTEGER REFERENCES customers(cust_id), order_dttm timestamp, status varchar(20));")
cur.execute("INSERT INTO orders (cust_id, order_dttm, status) VALUES (%s, %s, %s)", ("1", "2004-10-19 10:23:54", "OK"))
cur.execute("INSERT INTO orders (cust_id, order_dttm, status) VALUES (%s, %s, %s)", ("2", "2004-10-20 11:43:21", "OK"))

cur.execute("CREATE TABLE goods (good_id serial PRIMARY KEY, vendor varchar(100), name varchar(100), description varchar(300));")
cur.execute("INSERT INTO goods (vendor, name, description) VALUES (%s, %s, %s)", ("A.V.Egorov", "Drugs", "5s"))
cur.execute("INSERT INTO goods (vendor, name, description) VALUES (%s, %s, %s)", ("A.M.Sidorov", "Potato", "5s"))

cur.execute("CREATE TABLE order_items(order_item_id serial PRIMARY KEY, order_id INTEGER REFERENCES orders(order_id), good_id INTEGER REFERENCES goods(good_id), quantity INTEGER);")
cur.execute("INSERT INTO order_items (order_id, good_id, quantity) VALUES (%s, %s, %s)", ("1", "2", "4"))
cur.execute("INSERT INTO order_items (order_id, good_id, quantity) VALUES (%s, %s, %s)", ("2", "1", "1008"))

cur.execute("SELECT * FROM customers;")
print(cur.fetchall())
cur.execute("SELECT * FROM orders;")
print(cur.fetchall())
cur.execute("SELECT * FROM goods;")
print(cur.fetchall())
cur.execute("SELECT * FROM order_items;")
print(cur.fetchall())

conn.commit()

cur.close()
conn.close()

