import psycopg2

def insert_item(cur, order_id, good_id, quantity):
    try:
        cur.execute("INSERT INTO order_items (order_id, good_id, quantity) VALUES (%s, %s, %s)", (order_id, good_id, quantity))
    except:
        print("Exception! Check your ids.")


def delete_item(cur, del_order_item_id):
    try:
        cur.execute("DELETE FROM order_items WHERE order_item_id = "+del_order_item_id)
    except:
        print("Exception! Check your id.")


def update_item_quantity(cur, upd_order_item_id, new_quantity):
    try:
        cur.execute("UPDATE order_items SET quantity = "+new_quantity+"WHERE order_item_id = "+upd_order_item_id)
    except:
        print("Exception! Check your id.")


conn = psycopg2.connect(host = "localhost", database="shop_data", user="postgres", password="qwerty")

cur = conn.cursor()

print("INSERTION!")
print("Enter order_id, good_id, quantity")
order_id, good_id, quantity = input().split(' ')
insert_item(cur, order_id, good_id, quantity)

print("DELETION!")
print("Enter order_item_id")
order_item_id = input()
delete_item(cur, order_item_id)

print("UPDATE!")
print("Enter order_item_id, quantity")
order_item_id, quantity = input().split(' ')
update_item_quantity(cur, order_item_id, quantity)

conn.commit()

cur.close()
conn.close()

