#!/bin/bash

/opt/prometheus/prometheus --config.file=/opt/prometheus/prometheus.yml \
	--web.console.libraries=/opt/prometheus/console_libraries \
	--web.console.templates=/opt/prometheus/consoles
