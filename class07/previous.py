from peewee import *

db =  PostgresqlDatabase(database="shop_data", user="postgres", password="qwerty")


class Customer(Model):
    cust_id = AutoField(primary_key=True)
    first_nm = CharField()
    last_nm = CharField()

    class Meta:
        database = db


class Order(Model):
    order_id = AutoField(primary_key=True)
    cust_id = ForeignKeyField(Customer)
    order_dttm = DateTimeField()
    status = CharField()

    class Meta:
        database = db


class Good(Model):
    good_id = AutoField(primary_key=True)
    vendor = CharField() 
    name = CharField()
    description = CharField()

    class Meta:
        database = db


class OrderItem(Model):
    order_item_id = AutoField(primary_key=True)
    order_id = ForeignKeyField(Order)
    good_id = ForeignKeyField(Good)
    quantity = IntegerField() 

    class Meta:
        database = db


def drop_all():
    OrderItem.drop_table()
    Good.drop_table()
    Order.drop_table()
    Customer.drop_table()

def main():
    Customer.create_table()
    c1 = Customer.create(first_nm = "Vanya", last_nm = "Ivanov")
    c2 = Customer.create(first_nm = "Anton", last_nm = "Eotov")
    c1.save()
    c2.save()

    Order.create_table()
    o1 = Order.create(cust_id = "1", order_dttm = "2004-10-19 10:23:54", status = "OK")
    o2 = Order.create(cust_id = "2", order_dttm = "2004-10-20 11:43:21", status = "OK")
    o1.save()
    o2.save()

    Good.create_table()
    g1 = Good.create(vendor = "A.V.Egorov", name = "Drugs", description = "5s")
    g2 = Good.create(vendor = "A.M.Sidorov", name = "Potato", description = "5s")
    g1.save()
    g2.save()

    OrderItem.create_table()
    oi1 = OrderItem.create(order_id = "1", good_id = "2", quantity = "4")
    oi2 = OrderItem.create(order_id = "2", good_id = "1", quantity = "1008")
    oi1.save()
    oi2.save()


def show_all():
    for customer in Customer.select():
        print(customer.first_nm, customer.last_nm)
    for order in Order.select():
        print(order.cust_id, order.order_dttm, order.status)
    for good in Good.select():
        print(good.vendor, good.name, good.description)
    for order_item in OrderItem.select():
        print(order_item.order_id, order_item.good_id, order_item.quantity)


def insert_item(new_order_id, new_good_id, new_quantity):
    try:
        o = OrderItem.create(order_id = new_order_id, good_id = new_good_id, quantity = new_quantity)
        o.save()
    except:
        print("Exception! Check your ids")


def delete_item(del_order_item_id):
    try:
        oi = OrderItem.get(OrderItem.order_item_id == del_order_item_id)
        oi.delete_instance()
        #OrderItem.delete().where(OrderItem.order_item_id = del_order_item_id)
    except:
        print("Exception! Check your ids")


def update_item_quantity(upd_order_item_id, new_quantity):
    try:
        for order_item in OrderItem.select().where(OrderItem.order_item_id == upd_order_item_id):
            order_item.quantity = new_quantity
            order_item.save()
    except:
        print("Exception! Check your ids")


def write_csv():
    f = open('db.csv', 'w')
    f.write("order_item_id,order_id,quantity,first_nm,last_nm,name,vendor\n") 
    Query = (OrderItem.select(OrderItem.order_item_id, OrderItem.order_id, OrderItem.quantity, Customer.first_nm, Customer.last_nm, Good.name, Good.vendor).join(Good).switch(OrderItem).join(Order).join(Customer).objects())
    for q in Query:
        f.write(str(q.order_item_id)+","+str(q.order_id)+","+str(q.quantity)+","+str(q.first_nm)+","+str(q.last_nm)+","+str(q.name)+","+str(q.vendor)+"\n")


drop_all() #for tests
main()
insert_item(1, 1, 1000)
delete_item(1)
update_item_quantity(2, 999)
write_csv()
#show_all()
